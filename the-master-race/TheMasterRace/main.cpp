//**Project Information**//
///The Master Race
///Debug Code  : 0.0.1
///Programmers : Suciu Calin, Tom Zajac
///Game state  : Unplayable
///PI END
//*********************//
///Library and Header Files
#include<windows.h>
#include<SFML/Graphics.hpp>
#include<SFML/Audio.hpp>
#include<string>
///L AND H FILES END
///Namespaces
using namespace std;
///End Namespace
///Game Code
namespace Game
{
    //sf::Texture texture;
    sf::Texture CreateTexture(string texture_path)
    {
        sf::Texture retTexture;
        retTexture.loadFromFile(texture_path);
        return retTexture;
    }
    void draw(sf::RenderWindow &sf_wind_obj,sf::Sprite texture)
    {
        sf_wind_obj.clear();
        sf_wind_obj.draw(texture);
        sf_wind_obj.display();
    }
}
///End Game Code
///WinApi Functions
LRESULT CALLBACK WndProc(HWND hwnd,UINT msg,WPARAM wp,LPARAM lp)
{
    switch(msg)
    {
    case WM_CLOSE :
        DestroyWindow(hwnd);
        break;
    case WM_DESTROY :
        PostQuitMessage(1);
        break;
    default :
        return DefWindowProc(hwnd,msg,wp,lp);
        break;
    }
}
int WINAPI WinMain(HINSTANCE hinst, HINSTANCE hinst_no_use,LPSTR lpstr, int INT)
{
    char       class_name[] = "The Master Race";
    HWND       hwnd;
    MSG        msg;
    WNDCLASSEX wnd;
    wnd.cbClsExtra    = 0;
    wnd.cbSize        = sizeof(WNDCLASSEX);
    wnd.cbWndExtra    = 0;
    wnd.hbrBackground = (HBRUSH)(COLOR_WINDOW + 25); // Fill Window with Color
    wnd.hCursor       = LoadCursor(hinst,IDC_HAND); // Will update with custom art
    wnd.hIcon         = LoadIcon  (hinst,IDI_APPLICATION); // Same as above
    wnd.hIconSm       = LoadIcon  (hinst,IDI_APPLICATION); // Same as above
    wnd.hInstance     = hinst;
    wnd.lpfnWndProc   = WndProc;
    wnd.lpszClassName = class_name;
    wnd.lpszMenuName  = NULL;
    wnd.style         = 0;
    RegisterClassEx(&wnd);
    FreeConsole(); // We don't want that annyoing console hanging around, don't we ?
    hwnd              = CreateWindowEx(WS_EX_CLIENTEDGE,class_name,"The Master Race dev_build 0.0.1",WS_OVERLAPPEDWINDOW,CW_USEDEFAULT,CW_USEDEFAULT,1280,720,NULL,NULL,hinst,NULL);
    sf::RenderWindow sf_wind_obj(hwnd);
    sf::Texture ACtexture = Game::CreateTexture("ass.jpg");
    sf::Sprite texture_render(ACtexture);
    ShowWindow(hwnd,SW_SHOWNORMAL);
    UpdateWindow(hwnd);
    while(msg.message != WM_QUIT)
    {
        if(PeekMessage(&msg,NULL,0,0,PM_REMOVE))
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
        else
        {
            //Game Code here
            Game::draw(sf_wind_obj,texture_render);
        }
    }
}

